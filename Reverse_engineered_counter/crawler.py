#!Doctype .py
#imports
import csv
import requests
import re
from datetime import datetime

from bs4 import BeautifulSoup

def addNew(word, dictionary):
    # Add word to dictionanry
    if (word in dictionary):
        dictionary[word] = dictionary[word] + 1
    else:
        dictionary[word] = 1
        
    return dictionary

def createCSV(dictionary, filename):
    # Write new .csv file
	with open(filename, "w", newline='', encoding='UTF-8') as file:
		writer = csv.writer(file, delimiter=';')
		writer.writerows(dictionary.items())
	file.close()
	
def web(page, WebUrl, dictionary):
    # Gets web page content and find all links
    if(page > 0):
        url = WebUrl
        code = requests.get(url)
        plainText = code.text
        page = BeautifulSoup(plainText, "html.parser")

        for text in page.findAll('a', {}, limit=None):
            word = text.getText().rstrip().upper()
            dictionary = addNew(word, dictionary)

        return dictionary

def getFileContents(filename):
    # Get list of webpages
    with open(filename) as file:
        content = file.readlines()
        content = [line.strip() for line in content]
        for line in content:
            line = line.strip()
            print("Found:\t" + line)
    file.close()
    return content

def writeLog(dictionary):
    # Write log
    with open('logFile.txt', 'w', newline='', encoding='UTF-8') as file:
        for item in dictionary:
            file.write(item + ' ')
    file.close()

def main():
    # Create dicitionarys
    allContentDict = {}
    wordDictionary = {}
    sentenceDictionary = {}

    # Get content
    pageList = getFileContents("sources.txt")
    for item in pageList:
        print("Working with:\t" + item)
        allContentDict = web(1, item, allContentDict)

    # Analyze
    print('Analyzing...')
    for key in allContentDict:
        #key = re.sub('[^A-Za-z0-9äüöÄÜÖ]+', ' ', key)
        key.strip()
        wordlist = key.split()
        isSentence = False
        for word in wordlist:
            word.replace('"', '')
            if len(wordlist) > 1:
                isSentence = True
            if re.sub('[^A-Za-zäöÄÖ]+', '', word):
                wordDictionary = addNew(word, wordDictionary)
            else:
                wordlist.remove(word)
        if isSentence:
            sentence = ''
            for word in wordlist:
                sentence = sentence + ' ' + word
            sentence.strip()
            sentenceDictionary = addNew(sentence, sentenceDictionary)                

    # Create CSVs
    print('Creating logs...')
    path = datetime.now().strftime('outputs/%H-%d-%m-%Y-words-output.csv')     
    createCSV(wordDictionary, path)
    path = datetime.now().strftime("outputs/%H-%d-%m-%Y-sentences-output.csv") 
    createCSV(sentenceDictionary, path)
    writeLog(wordDictionary)
    
    print('Completed')
    

main()

